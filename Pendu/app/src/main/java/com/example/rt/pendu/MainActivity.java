package com.example.rt.pendu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button mButton;
    private TextView mText;
    private EditText mLettre;
    private ImageView mImage;
    private int mGoodAnswer;
    private String[] mMots = {"LYS", "VOIE", "RUE", "SAC", "SAVATE", "PANNEAU", "CHAT", "LUNE", "CANARD", "TURQUOISE", "TROUBLE"};
    private int mErreurs;
    private String mAffiche;
    private String mReponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButton = (Button) findViewById(R.id.button);
        mText = (TextView) findViewById(R.id.mot);
        mLettre = (EditText) findViewById(R.id.lettre);
        mImage = (ImageView) findViewById(R.id.image);
        Random rand = new Random();
        mGoodAnswer = rand.nextInt(10) + 0;
        mReponse = mMots[mGoodAnswer];
        newGame();

        mButton.setOnClickListener(myClickListener);
    }

    public void newGame(){
        mButton.setText("Vérif");
        mText.setText("");
        mErreurs = 0;
        mImage.setImageResource(getResources().getIdentifier("pendu_tp_"+mErreurs, "drawable", getPackageName()));
        for(int i=0; i<mReponse.length(); i++ ){
            mText.setText(String.valueOf(mText.getText())+"_ ");
        }

    }

    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (mButton.getText().toString()) {
                case "Vérif":
                    click();
                    break;
                case "Recommencer":
                    Random rand = new Random();
                    mGoodAnswer = rand.nextInt(10) + 0;
                    mReponse = mMots[mGoodAnswer];
                    newGame();
                    break;
                case "Valider":
                    mReponse = mLettre.getText().toString();
                    mLettre.setText("");
                    newGame();
                    break;
                default:
            }
        }
    };

    public void click() {
        int n = mReponse.length();
        mAffiche =  mText.getText().toString();
        if(String.valueOf(mLettre.getText())!= null && !String.valueOf(mLettre.getText()).isEmpty()){
            if(String.valueOf(mLettre.getText()).length()  != mReponse.length()){
                if(String.valueOf(mLettre.getText()).length() == 1){
                    for(int i=0; i<mReponse.length(); i++ ){
                        if (String.valueOf(mLettre.getText()).equals(String.valueOf(mReponse.charAt(i)))) {
                            mAffiche = mAffiche.substring(0, i*2) + String.valueOf(mReponse.charAt(i)) + mAffiche.substring(i*2 + 1);
                            n--;
                        }
                    }
                }
                if(mAffiche.contains("_") == false){
                    mButton.setText("Recommencer");
                    mText.setText("BRAVO! Le mot: "+mReponse);
                    return;
                }
            }else{
                if(String.valueOf(mLettre.getText()).equals(mReponse)){
                    mButton.setText("Recommencer");
                    mText.setText("BRAVO! Le mot: "+mReponse);
                    return;
                }
            }
            mText.setText(mAffiche);
            if(n == mReponse.length()){
                mErreurs++;
                mImage.setImageResource(getResources().getIdentifier("pendu_tp_"+mErreurs, "drawable", getPackageName()));
                if(mErreurs==5){
                    mButton.setText("Recommencer");
                    mText.setText("Dommage! Le mot: "+mReponse);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.solution:
                mText.setText("La réponse est "+mReponse+"!");
                mButton.setText("Recommencer");
                break;
            case R.id.versus:
                mErreurs = 0;
                mImage.setImageResource(getResources().getIdentifier("pendu_tp_"+mErreurs, "drawable", getPackageName()));
                mText.setText("Choisi ton mot:");
                mButton.setText("Valider");
                break;
            default:
        }
        return true;
    }
}
