package com.example.rt.webview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private WebView mWebView;
    private Button mButton;
    private EditText mText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = (WebView) findViewById(R.id.webview);
        mButton = (Button) findViewById(R.id.button);
        mText = (EditText) findViewById(R.id.editText);

        mButton.setOnClickListener(myClickListener);


    }

    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mWebView.loadUrl(mText.getText().toString());
        }
    };

}
