package com.example.rt.plusoumoins;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button mButton;
    private TextView mText;
    private EditText mReponse;
    private int mGoodAnswer;
    private String mString;
    private TextView mCounter;
    private int mCoups;
  //  private boolean mNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButton = (Button) findViewById(R.id.button);
        mText = (TextView) findViewById(R.id.text);
        mReponse = (EditText) findViewById(R.id.editText);
        Random rand = new Random();
        mGoodAnswer = rand.nextInt(100) + 1;
        mCounter = (TextView) findViewById(R.id.counter);
        mCoups = 0;

        mButton.setOnClickListener(myClickListener);
    }

    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (mButton.getText().toString()) {
                case "Vérif":
                    mCoups++;
                    mCounter.setText(mCoups + " coups");
                    click();
                break;
                case "Recommencer":
                    Random rand = new Random();
                    mGoodAnswer = rand.nextInt(100) + 1;
                    newGame();
                break;
                case "Valider":
                    mGoodAnswer = Integer.parseInt(String.valueOf(mReponse.getText()));
                    mReponse.setText("");
                    newGame();
                break;
                default:
            }

        }
    };

    public void click() {
        if(String.valueOf(mReponse.getText())!= null && !String.valueOf(mReponse.getText()).isEmpty()){
            if(Integer.parseInt(String.valueOf(mReponse.getText())) != mGoodAnswer) {
                if (Integer.parseInt(String.valueOf(mReponse.getText())) < mGoodAnswer) {
                    mString = " + Plus + ";
                } else {
                    mString = " - Moins - ";
                }
            }else{
                mString = "Bravo!";
                mButton.setText("Recommencer");
            }
        }else{
            mString = "Veuillez choisir un nombre s'il vous plaît";
        }
        mText.setText(mString);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.solution:
                mText.setText("La réponse est "+mGoodAnswer+"!");
                mButton.setText("Recommencer");
                break;
            case R.id.versus:
                mCounter.setText("");
                mReponse.setText("");
                mText.setText("Choisi ton nombre");
                mButton.setText("Valider");
                mReponse.setHint("Saisi ton nombre");
                break;
            default:
        }
        return true;
    }


    public void newGame(){
        mCoups = 0;
        mCounter.setText(mCoups + " coups");
        mText.setText("Devine mon nombre! (Entre 1 et 100)");
        mButton.setText("Vérif");
        mReponse.setHint("Saisi ton nombre");

    }
}
