package com.example.rt.td2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity{

    private Button mButton;
    private TextView mText;
    private int mCounter;
    private boolean mCroissant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButton = (Button) findViewById(R.id.button);
        mText = (TextView) findViewById(R.id.text);
        mCounter = 0;

        mButton.setOnClickListener(myClickListener);
        mCroissant = true;
    }

    public void click() {
        if (mCroissant == true){
            mCounter++;
        }else{
            mCounter--;
        }

        mText.setText(String.valueOf(mCounter));
    }

    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            click();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return mCroissant;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_direction:
                if (mCroissant==false){
                    mCroissant = true;
                }else{
                    mCroissant = false;
                }
                break;
            case R.id.reset:
                mCroissant = true;
                mCounter = 0;
                mText.setText("Et on recommence!");
                break;
            default:
        }
        return mCroissant;
    }
}
